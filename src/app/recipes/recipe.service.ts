import {EventEmitter} from "@angular/core";
import {Recipe} from "./recipe.model";

export class RecipeService {

    recipeSelected = new EventEmitter<Recipe>();

    private recipes: Recipe[] = [
        new Recipe (
            "Test Recipe 01",
            "This is test roasted meat",
            "https://static.pexels.com/photos/161640/abstract-barbecue-barbeque-bbq-161640.jpeg"),
        new Recipe (
            "Test Recipe 02",
            "This is test 02",
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSc9HAcnThAZkd2Ka3zX1UNzfHNizTz052CC5S_s1zdtDUCkZpI")
    ];

    getRecipes() {
        return this.recipes.slice();
    }

}